/*
 * RevAbstractBeatDetector.h
 *
 *  Created on: 05-11-2012
 *      Author: Revers
 */

#ifndef REVABSTRACTBEATDETECTOR_H_
#define REVABSTRACTBEATDETECTOR_H_

#include <rev/audio/RevAudioFormat.h>
#include "RevIBeatListener.h"
#include "RevIDSP.h"

namespace rev {
    class AbstractBeatDetector: public IDSP {
    protected:

        IBeatListener* beatListener;

    public:
        AbstractBeatDetector(IBeatListener* beatListener) :
                beatListener(beatListener) {
        }

        virtual ~AbstractBeatDetector() {
        }

        virtual bool init(AudioFormat audioFormat, int framesAtOnce) = 0;

        virtual void process(float* leftData, float* rightData,
                int dataSize) override = 0;

        virtual void reset() = 0;
    };
}

#endif /* REVABSTRACTBEATDETECTOR_H_ */
