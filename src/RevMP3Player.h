/*
 * RevMP3Player.h
 *
 *  Created on: 09-10-2012
 *      Author: Revers
 */

#ifndef REVMP3PLAYER_H_
#define REVMP3PLAYER_H_

#include <memory>
#include <iostream>
#include <vector>

#include <rev/audio/RevIAudioSeekableInputStream.h>
#include <rev/audio/RevIAudioOutputStream.h>

#include "RevIPlayerListener.h"
#include "RevDSPAudioDataConsumer.h"

namespace boost {
class mutex;
class condition_variable_any;
}

namespace rev {

typedef std::shared_ptr<IAudioSeekableInputStream> IAudioSeekableInputStreamPtr;

class MP3Player {

	enum class State {
		NOT_INITED, STOPPED, PLAYING, PAUSED, SEEKING
	};

	boost::mutex* pauseMutex = nullptr;
	boost::mutex* seekMutex = nullptr;
	boost::condition_variable_any* pauseCondition = nullptr;

	IAudioSeekableInputStreamPtr audioInputStream;

	State state = State::NOT_INITED;
	int bufferSizeInFrames = 0;

	IPlayerListener* listener = nullptr;

	DSPAudioDataConsumer consumer;

	DSPVector dspVector;
	int bufferSizeInBytes = 0;
	unsigned char* buffer = nullptr;

public:

	MP3Player(IAudioOutputStreamPtr outputStreamPtr);

	virtual ~MP3Player();

	bool open(const char* filename, int bufferSizeInFrames = 1024);

	bool play();

	bool pause();

	bool stop();

	void seek(int samplePos);

	float getTrackLenghtMs() {
		return (float) audioInputStream->getLength() * 1000.0f
				/ audioInputStream->getFormat().sampleRate;
	}

	float getMsPerSample() {
		return 1000.0f / (float) audioInputStream->getFormat().sampleRate;
	}

	int getTrackLengthSamples() {
		return audioInputStream->getLength();
	}

	AudioFormat getFormat() {
		return audioInputStream->getFormat();
	}

	int getCurrentSamplePos() {
		return audioInputStream->getPosition();
	}

	IPlayerListener* getListener() {
		return listener;
	}

	void setListener(IPlayerListener* listener) {
		this->listener = listener;
	}

	DSPVector& getDSPVector() {
		return consumer.getDSPVector();
	}

	void addDSP(rev::IDSP* dsp) {
		consumer.addDSP(dsp);
	}

	IDSP* getLastDSP() {
		return consumer.getLastDSP();
	}

	void setLastDSP(IDSP* lastDSP) {
		consumer.setLastDSP(lastDSP);
	}

	bool isStopped() {
		return state == State::STOPPED || state == State::NOT_INITED;
	}

	bool isPaused() {
		return state == State::PAUSED;
	}

	bool isPlaying() {
		return state == State::PLAYING;
	}

	int getFramesAtOnce() {
		return bufferSizeInFrames;
	}

private:
	void run();
	void printState(const char* func, int line);

	const char* func(const char* f) {
		return f;
	}
};

typedef std::shared_ptr<rev::MP3Player> MP3PlayerPtr;
} /* namespace rev */
#endif /* REVMP3PLAYER_H_ */
