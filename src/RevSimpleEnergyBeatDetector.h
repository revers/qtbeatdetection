/*
 * RevSimpleEnergyBeatDetector.h
 *
 *  Created on: 05-11-2012
 *      Author: Revers
 */

#ifndef REVSIMPLEENERGYBEATDETECTOR_H_
#define REVSIMPLEENERGYBEATDETECTOR_H_

#include <cstring>
#include <rev/common/RevAssert.h>
#include "RevAbstractBeatDetector.h"

namespace rev {

    class SimpleEnergyBeatDetector: public AbstractBeatDetector {
        float* historyBuffer = nullptr;
        int historyBufferSize = -1;
        int packetsRead = 0;
        AudioFormat format;
        float coefficientA = -0.0025714f;
        float coefficientB = 1.5152857f;//1.5142857f;

    public:
        SimpleEnergyBeatDetector(IBeatListener* beatListener) :
                AbstractBeatDetector(beatListener) {
        }

        virtual ~SimpleEnergyBeatDetector() {
            if (historyBuffer) {
                delete[] historyBuffer;
            }
        }

        void process(float* leftData, float* rightData,
                int dataSize) override;

        bool init(AudioFormat format, int framesAtOnce) override;

        void reset() override {
            packetsRead = 0;
        }

        float getCoefficientA() const {
            return coefficientA;
        }

        void setCoefficientA(float coefficientA) {
            this->coefficientA = coefficientA;
        }

        float getCoefficientB() const {
            return coefficientB;
        }

        void setCoefficientB(float coefficientB) {
            this->coefficientB = coefficientB;
        }
    };

} /* namespace rev */
#endif /* REVSIMPLEENERGYBEATDETECTOR_H_ */
