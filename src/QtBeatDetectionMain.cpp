/*
 * File:   TestQtAppMain.cpp
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#include <iostream>
#include <QDesktopWidget>
#include <QApplication>

#include <rev/audio/RevIAudioOutputStream.h>
#include <rev/audio/RevAudioFactory.h>
#include <rev/common/RevErrorStream.h>

#include "revqt/QtBeatDetectionWindow.h"

void center(QWidget &widget) {

    int width = widget.width();
    int height = widget.height();

    QDesktopWidget* desktop = QApplication::desktop();

    int screenWidth = desktop->width();
    int screenHeight = desktop->height();

    int x = (screenWidth - width) / 2;
    int y = (screenHeight - height) / 2;

    widget.setGeometry(x, y, width, height);
}

int main(int argc, char *argv[]) {

    rev::IAudioOutputStreamPtr audioOutputPtr =
            rev::AudioFactory::getDefaultAudioOutputStream();

    if (audioOutputPtr == nullptr) {
        rev::errout << "ERROR: cannot create audio output stream!!" << std::endl;
        return -1;
    }

    QApplication app(argc, argv);
    app.setApplicationName("QtBeatDetection");
    app.setOrganizationName("revers");

    QtBeatDetectionWindow window(audioOutputPtr);
    window.show();
    center(window);

    return app.exec();
}
