/*
 * RevSimpleEnergyBeatDetector.cpp
 *
 *  Created on: 05-11-2012
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevSimpleEnergyBeatDetector.h"

using namespace rev;

#define HISTORY_BUFFER_MULTIPLIER 4

void SimpleEnergyBeatDetector::process(float* leftData, float* rightData,
        int dataSize) {
    assert(historyBufferSize);

    float instantEnergy = 0.0f;
    for (int i = 0; i < dataSize; i++) {
        instantEnergy += leftData[i] * leftData[i]
                + rightData[i] * rightData[i];
    }
    instantEnergy /= dataSize;

    if (packetsRead < historyBufferSize) {
        historyBuffer[packetsRead++] = instantEnergy;
        return;
    }

    float* historyBegin = historyBuffer + packetsRead
            - historyBufferSize;

    float averageLocalEnergy = 0.0f;
    for (int i = 0; i < historyBufferSize; i++) {
        averageLocalEnergy += historyBegin[i];
    }

    averageLocalEnergy /= historyBufferSize;

    float variance = 0.0f;
    for (int i = 0; i < historyBufferSize; i++) {
        float diff = historyBegin[i] - averageLocalEnergy;
        variance += diff * diff;
    }
    variance /= historyBufferSize;
    float c = (coefficientA * variance) + coefficientB;

    if (packetsRead < HISTORY_BUFFER_MULTIPLIER * historyBufferSize) {
        historyBuffer[packetsRead++] = instantEnergy;
    } else {
        memcpy(historyBuffer, historyBuffer + historyBufferSize + 1,
                (historyBufferSize - 1) * sizeof(float));
        historyBuffer[historyBufferSize - 1] = instantEnergy;
        packetsRead = historyBufferSize;
    }

    if (instantEnergy > c * averageLocalEnergy) {
        beatListener->beat(leftData, rightData, dataSize);
    }
}

bool SimpleEnergyBeatDetector::init(AudioFormat format, int framesAtOnce) {
    this->format = format;
    historyBufferSize = (int) ((float) format.sampleRate * 1.5f / framesAtOnce
            + 0.5f);

    historyBuffer = new float[historyBufferSize * HISTORY_BUFFER_MULTIPLIER];

    REV_TRACE_MSG("History buffer size: " << historyBufferSize);

    return true;
}

