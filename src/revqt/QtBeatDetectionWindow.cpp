/*
 * File:   QtBeatDetectionWindow.cpp
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#include <iostream>

#include <QCloseEvent>
#include <QButtonGroup>
#include <QSizePolicy>
#include <QEvent>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QMouseEvent>
#include <QSettings>
#include <QStackedWidget>

#include <rev/common/RevAssert.h>
#include <rev/common/RevSleep.h>

#include "QtBeatDetectionWindow.h"
#include "SignalAmplitudeGraph.h"
#include "SignalSpectrumGraph.h"
#include "../RevSimpleEnergyBeatDetector.h"
#include "../RevFrequencySelectedBeatDetector.h"

using namespace std;

#define HOUR (1000 * 60 * 60)
#define MINUTE (1000 * 60)
#define SECOND 1000

#define FRAMES_AT_ONCE 2048

QtBeatDetectionWindow::QtBeatDetectionWindow(
        rev::IAudioOutputStreamPtr audioOutputPtr) :
        player(audioOutputPtr) {

    widget.setupUi(this);

    signalAmplitudeGraph = new rev::SignalAmplitudeGraph(this);
    simpleEnergyBeatDetector = new rev::SimpleEnergyBeatDetector(
            signalAmplitudeGraph);
    frequencySelectedBeatDetector = new rev::FrequencySelectedBeatDetector(
            signalAmplitudeGraph);
    // signalSpectrumGraph = new rev::SignalSpectrumGraph(this);

    widget.visualizationLayout->addWidget(signalAmplitudeGraph);
    //  widget.visualizationLayout->addWidget(signalSpectrumGraph);

    player.addDSP(simpleEnergyBeatDetector);
    player.setLastDSP(signalAmplitudeGraph);

    widget.visualizationLayout->setStretchFactor(signalAmplitudeGraph, 666666);
    signalAmplitudeGraph->setSizePolicy(QSizePolicy::Expanding,
            QSizePolicy::Expanding);

    QButtonGroup* algorithmsGroup = new QButtonGroup(this);
    algorithmsGroup->addButton(widget.simpleEnergyRB);
    algorithmsGroup->addButton(widget.frequencySelectedEnergyRB);
    algorithmsGroup->addButton(widget.filteringRhythmRB);

    player.setListener(this);

    widget.playButton->setDisabled(true);
    widget.pauseButton->setDisabled(true);
    widget.stopButton->setDisabled(true);

    widget.progressSlider->installEventFilter(this);

    widget.simpleCoeffASB->setValue(simpleEnergyBeatDetector->getCoefficientA());
    widget.simpleCoeffBSB->setValue(simpleEnergyBeatDetector->getCoefficientB());

    widget.freqCoeffASB->setValue(frequencySelectedBeatDetector->getCoefficientA());
    widget.freqCoeffBSB->setValue(frequencySelectedBeatDetector->getCoefficientB());
    widget.bandsL->setText(QString::number(frequencySelectedBeatDetector->getBands()));
    widget.freqThresholdSB->setValue(
            frequencySelectedBeatDetector->getBandThreshold());

    setupActions();

    widget.frequencySelectedEnergyRB->setChecked(true);

    resize(1044, 589);
}

QtBeatDetectionWindow::~QtBeatDetectionWindow() {
    if (simpleEnergyBeatDetector) {
        delete simpleEnergyBeatDetector;
    }

    if (frequencySelectedBeatDetector) {
        delete frequencySelectedBeatDetector;
    }
}

void QtBeatDetectionWindow::beatDetectionRadioChecked(bool checked) {

    rev::DSPVector& dsps = player.getDSPVector();
    dsps.clear();

    if (widget.simpleEnergyRB->isChecked()) {
        REV_TRACE_MSG("SimpleEnergyBeatDetector " << boolalpha << checked);
        dsps.push_back(simpleEnergyBeatDetector);
    } else if (widget.frequencySelectedEnergyRB->isChecked()) {
        REV_TRACE_MSG("FrequencySelecteBeatDetector " << boolalpha << checked);
        dsps.push_back(frequencySelectedBeatDetector);
    } else if (widget.filteringRhythmRB->isChecked()) {
        REV_TRACE_MSG("FilteringRhythmBeatDetector " << boolalpha << checked);
    }
}

void QtBeatDetectionWindow::setupActions() {

    connect(widget.actionExit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

    connect(widget.actionOpen, SIGNAL(triggered(bool)),
            this, SLOT(openAction()));

    connect(widget.openButton, SIGNAL(clicked()),
            this, SLOT(openAction()));

    connect(widget.actionAbout, SIGNAL(triggered(bool)),
            this, SLOT(aboutAction()));

    connect(widget.simpleEnergyRB, SIGNAL(toggled(bool)),
            this, SLOT(beatDetectionRadioChecked(bool)));

    connect(widget.frequencySelectedEnergyRB, SIGNAL(toggled(bool)),
            this, SLOT(beatDetectionRadioChecked(bool)));

    connect(widget.filteringRhythmRB, SIGNAL(toggled(bool)),
            this, SLOT(beatDetectionRadioChecked(bool)));

    connect(widget.playButton, SIGNAL(clicked()),
            this, SLOT(playAction()));

    connect(widget.pauseButton, SIGNAL(clicked()),
            this, SLOT(pauseAction()));

    connect(widget.stopButton, SIGNAL(clicked()),
            this, SLOT(stopAction()));

    connect(widget.progressSlider, SIGNAL(valueChanged(int)),
            this, SLOT(progressSliderValueChangedEvent(int)), Qt::QueuedConnection);

    connect(this, SIGNAL(stopSignal()), this, SLOT(stopSlot()), Qt::QueuedConnection);

    connect(this, SIGNAL(pauseSignal()), this, SLOT(pauseSlot()), Qt::QueuedConnection);

    connect(this, SIGNAL(playSignal()), this, SLOT(playSlot()), Qt::QueuedConnection);

    connect(this, SIGNAL(timeSignal(unsigned int)), this, SLOT(timeSlot(unsigned int)), Qt::QueuedConnection);

    // connect(this, SIGNAL(repaintGraphsSignal()), signalSpectrumGraph, SLOT(requestRepaint()), Qt::QueuedConnection);
    connect(this, SIGNAL(repaintGraphsSignal()), signalAmplitudeGraph, SLOT(requestRepaint()), Qt::QueuedConnection);

    connect(widget.simpleCoeffASB, SIGNAL(valueChanged(double)), this, SLOT(simpleCoeffAChanged(double)));
    connect(widget.simpleCoeffBSB, SIGNAL(valueChanged(double)), this, SLOT(simpleCoeffBChanged(double)));

    connect(widget.freqCoeffASB, SIGNAL(valueChanged(double)), this, SLOT(freqCoeffAChanged(double)));
    connect(widget.freqCoeffBSB, SIGNAL(valueChanged(double)), this, SLOT(freqCoeffBChanged(double)));

    connect(widget.freqThresholdSB, SIGNAL(valueChanged(int)), this, SLOT(freqBandsThresholdChanged(int)));
}

void QtBeatDetectionWindow::closeEvent(QCloseEvent* event) {

    player.stop();
    REV_TRACE_MSG("Stopping player...");

    do {
        rev::sleep(100);
    } while (!player.isStopped());
}

bool QtBeatDetectionWindow::eventFilter(QObject* watched, QEvent* e) {

    if (e->type() == QEvent::MouseButtonPress) {
        QMouseEvent* event = static_cast<QMouseEvent*>(e);
        progressSliderMousePressedEvent(event);
    } else if (e->type() == QEvent::MouseButtonRelease) {
        QMouseEvent* event = static_cast<QMouseEvent*>(e);
        progressSliderMouseReleasedEvent(event);
    }

    return QWidget::eventFilter(watched, e);
}

void QtBeatDetectionWindow::aboutAction() {
    QMessageBox::information(this, "About...", "Author: Kamil Kolaczynski");
}

void QtBeatDetectionWindow::openAction() {

    const QString DEFAULT_DIR_KEY("default_dir");

    QSettings mySettings;

    QString selfilter = tr("MP3 (*.mp3)");
    QString filename = QFileDialog::getOpenFileName(
            this,
            tr("Open file"),
            mySettings.value(DEFAULT_DIR_KEY).toString(),
            tr("MP3 (*.mp3)"),
            &selfilter);

    if (filename.isEmpty()) {
        return;
    }

    player.stop();

    while (!player.isStopped()) {
        rev::sleep(100);
    }

    QDir currentDir;
    mySettings.setValue(DEFAULT_DIR_KEY, currentDir.absoluteFilePath(filename));

    QFileInfo info(filename);
    REV_TRACE_MSG("filename = " << info.fileName().data() << "...");

    if (!player.open(filename.toAscii().data(), FRAMES_AT_ONCE)) {
        widget.titleL->setText(QString("ERROR: Cannot open: ") + info.fileName());

        widget.playButton->setDisabled(true);
        widget.pauseButton->setDisabled(true);
        widget.stopButton->setDisabled(true);
        return;
    }

    widget.titleL->setText(info.fileName());
    int ms = player.getTrackLenghtMs();
    msPerSample = player.getMsPerSample();

    if (ms / HOUR > 0) {
        gotHours = true;
    } else {
        gotHours = false;
    }

    if (ms / MINUTE > 0) {
        gotMinutes = true;
    } else {
        gotMinutes = false;
    }

    QString time = msToString(ms);
    widget.totalTimeL->setText(time);
    widget.progressSlider->setMinimum(0);
    widget.progressSlider->setValue(0);
    widget.progressSlider->setMaximum(player.getTrackLengthSamples() - 1);

    if (!simpleEnergyBeatDetector->init(player.getFormat(),
            player.getFramesAtOnce())) {
        REV_ERROR_MSG("ERROR: simpleEnergyBeatDetector->init() FAILED!!");
    }

    if (!frequencySelectedBeatDetector->init(player.getFormat(),
            player.getFramesAtOnce())) {
        REV_ERROR_MSG("ERROR: frequencySelectedBeatDetector->init() FAILED!!");
    }

    widget.bandsL->setText(QString::number(frequencySelectedBeatDetector->getBands()));
}

void QtBeatDetectionWindow::playAction() {
    REV_TRACE_MSG("playAction()");

    if (!player.play()) {
        REV_TRACE_MSG("player.play() FAILED");
    }
}

void QtBeatDetectionWindow::pauseAction() {
    REV_TRACE_MSG("pauseAction()");

    if (!player.pause()) {
        REV_TRACE_MSG("player.pause() FAILED");
    }
}

void QtBeatDetectionWindow::stopAction() {
    REV_TRACE_MSG("stopAction()");

    if (!player.stop()) {
        REV_TRACE_MSG("player.stop() FAILED");
    }
}

/**
 * @Override
 */
void QtBeatDetectionWindow::playEvent(rev::MP3Player* plr) {
    emit playSignal();
}

/**
 * @Override
 */
void QtBeatDetectionWindow::pauseEvent(rev::MP3Player* plr) {
    emit pauseSignal();
}

/**
 * @Override
 */
void QtBeatDetectionWindow::stopEvent(rev::MP3Player* plr) {
    emit stopSignal();
}

void QtBeatDetectionWindow::stopSlot() {
    REV_TRACE_MSG("stopEvent()");
    widget.playButton->setDisabled(false);
    widget.pauseButton->setDisabled(true);
    widget.stopButton->setDisabled(true);
}

void QtBeatDetectionWindow::pauseSlot() {
    REV_TRACE_MSG("pauseEvent()");
    widget.playButton->setDisabled(false);
    widget.pauseButton->setDisabled(true);
    widget.stopButton->setDisabled(false);
}

void QtBeatDetectionWindow::playSlot() {
    REV_TRACE_MSG("playEvent()");
    widget.playButton->setDisabled(true);
    widget.pauseButton->setDisabled(false);
    widget.stopButton->setDisabled(false);
}
//
//void QtBeatDetectionWindow::repaintGraphsSlot() {
//    signalAmplitudeGraph->repaint();
////    signalSpectrumGraph->repaint();
//
//    //widget.minFreqL->setText(QString::number(widget.minHzSlider->value()) + " Hz");
//    //widget.maxFreqL->setText(QString::number(widget.maxHzSlider->value()) + " Hz");
//}

QString QtBeatDetectionWindow::msToString(int millis) {
    int hours = millis / HOUR;
    millis -= hours * HOUR;
    int mins = millis / MINUTE;
    millis -= mins * MINUTE;
    int secs = millis / SECOND;
    millis -= secs * SECOND;

    QString time;
    time.reserve(16);

    if (gotHours || hours > 0) {
        if (hours < 10)
            time += "0";
        time += QString::number(hours);
        time += ":";
    }
    if (gotMinutes || mins > 0) {
        if (mins < 10)
            time += "0";
        time += QString::number(mins);
        time += ":";
    }

    if (secs < 10)
        time += "0";

    time += QString::number(secs);
    time += ".";

    if (millis < 10) {
        time += "00";
    } else if (millis < 100) {
        time += "0";
    }

    time += QString::number(millis);
    return time;
}

/**
 * @Override
 */
void QtBeatDetectionWindow::timeEvent(rev::MP3Player* plr,
        int currentSamplePos) {
    emit timeSignal(currentSamplePos);
}

void QtBeatDetectionWindow::timeSlot(unsigned int samplePos) {
    if (afterSeek) {
        afterSeek = false;
        return;
    }

    if (!sliderPressed) {
        widget.progressSlider->setValue(samplePos);
    }

    QString time = msToString((int) (msPerSample * samplePos));
    widget.currentTimeL->setText(time);
}

void QtBeatDetectionWindow::progressSliderMousePressedEvent(
        QMouseEvent* event) {
    sliderPressed = true;
}
void QtBeatDetectionWindow::progressSliderMouseReleasedEvent(
        QMouseEvent* event) {
    player.seek(sliderValue);
    afterSeek = true;
    sliderPressed = false;
}

//void QtBeatDetectionWindow::gainValueChanged(int value) {
//    float gain = 0.01f * value;
////    signalSpectrumGraph->setGain(gain);
////    widget.gainL->setText(QString::number(gain));
//
//    //if (!player.isPlaying()) {
//        emit repaintGraphsSignal();
//    //}
//}
//
//void QtBeatDetectionWindow::maximalFrequencyValueChanged(int value) {
//    widget.maxFreqL->setText(QString::number(value) + " Hz");
//
//    signalSpectrumGraph->setMaxFrequency(value);
//
//    if (!player.isPlaying()) {
//        emit repaintGraphsSignal();
//    }
//}
//
//void QtBeatDetectionWindow::minimalFrequencyValueChanged(int value) {
//    widget.minFreqL->setText(QString::number(value) + " Hz");
//
//    signalSpectrumGraph->setMinFrequency(value);
//
//    if (!player.isPlaying()) {
//        emit repaintGraphsSignal();
//    }
//}

void QtBeatDetectionWindow::progressSliderValueChangedEvent(int value) {
    if (!sliderPressed) {
        return;
    }
    sliderValue = value;

    QString time = msToString((int) (msPerSample * value));
    widget.currentTimeL->setText(time);

    if (!player.isPlaying()) {
        player.seek(sliderValue);
        afterSeek = true;
    }
}

void QtBeatDetectionWindow::simpleCoeffAChanged(double val) {
    simpleEnergyBeatDetector->setCoefficientA(val);
}

void QtBeatDetectionWindow::simpleCoeffBChanged(double val) {
    simpleEnergyBeatDetector->setCoefficientB(val);
}

void QtBeatDetectionWindow::freqCoeffAChanged(double val) {
    frequencySelectedBeatDetector->setCoefficientA(val);
}

void QtBeatDetectionWindow::freqCoeffBChanged(double val) {
    frequencySelectedBeatDetector->setCoefficientB(val);
}

void QtBeatDetectionWindow::freqBandsThresholdChanged(int val) {
    frequencySelectedBeatDetector->setBandThreshold(val);
}
