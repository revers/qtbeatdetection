/*
 * SignalAmplitudeGraph.cpp
 *
 *  Created on: 16-10-2012
 *      Author: Revers
 */

#include <iostream>
//#include <boost/thread.h>
#include <QTime>
#include <QApplication>
#include <QPainter>
#include <QImage>
#include <QRect>
#include <QTimerEvent>

#include "SignalAmplitudeGraph.h"

using namespace rev;
using namespace std;

#define IMAGE_WIDTH 1024
#define IMAGE_HEIGHT 256

SignalAmplitudeGraph::SignalAmplitudeGraph(QWidget* parent) :
        QWidget(parent), bgColor(203, 214, 230), circleColor(213, 224, 240) { //circleColor(0x53, 0x2C, 0x5E) {

    imageWidth = IMAGE_WIDTH;
    imageHeight = IMAGE_HEIGHT;
    imageBack = new QImage(imageWidth, imageHeight, QImage::Format_RGB32);
    imageFront = new QImage(imageWidth, imageHeight, QImage::Format_RGB32);
    cout << "creating image " << imageWidth << "x" << imageHeight << endl;

    QPainter painter(imageFront);
    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    painter.drawRect(0, 0, imageWidth - 1, imageHeight - 1);

    connect(this, SIGNAL(queuedRepaint()), this, SLOT(repaint()),
            Qt::QueuedConnection);

   // startTimer(500);
}

SignalAmplitudeGraph::~SignalAmplitudeGraph() {
    if (imageBack) {
        delete imageBack;
    }

    if (imageFront) {
        delete imageFront;
    }
}

void SignalAmplitudeGraph::timerEvent(QTimerEvent* event) {
    startCircleAnimation();
}

void SignalAmplitudeGraph::paintEvent(QPaintEvent* event) {

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawImage(QRect(0, 0, width() - 1, height() - 1), *imageFront);
}

#define MARGIN 5

void SignalAmplitudeGraph::requestRepaint() {

    drawBackImage();

    std::swap(imageFront, imageBack);
    emit queuedRepaint();
}

void SignalAmplitudeGraph::drawCircle(clock_t diffTime, QPainter& painter) {
    float percent = 1.0f - (float) diffTime / (float) animTotalTime + 0.2f;
    if (percent > 1.0f) {
        percent = 1.0f;
    }
    painter.setPen(circleColor);
    painter.setBrush(QBrush(circleColor));
    int circleDiameter = (int) (((float) imageHeight - 20.0f) * percent);
    int circleX = (imageWidth - circleDiameter) / 2;
    int circleY = (imageHeight - circleDiameter) / 2;
    painter.drawEllipse(circleX, circleY, circleDiameter, circleDiameter);
}

void SignalAmplitudeGraph::drawBackImage() {

    QPainter painter(imageBack);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    painter.drawRect(0, 0, imageWidth - 1, imageHeight - 1);

    //-------------------------------------------------------------
    clock_t diffTime = clock() - animStartTime;
    if (diffTime > 0 && diffTime <= animTotalTime) {
        drawCircle(diffTime, painter);
    }
    //=============================================================

    int graphHeight = imageHeight / 2 - 2 * MARGIN;
    int lastX = 0;
    int lastY = imageHeight / 4;

    painter.setPen(QPen(Qt::white, 1, Qt::SolidLine));

    int pointsPerPixel = dataSize / imageWidth;

    for (int i = 0; i < imageWidth; i++) {
        float val = 0;
        for (int j = 0; j < pointsPerPixel; j++) {
            val += (leftData[i * pointsPerPixel + j] * 0.5f + 0.5f) * graphHeight;
        }
        val /= pointsPerPixel;

        int x = i;
        int y = (int) val;

        painter.drawLine(lastX, lastY, x, y);
        lastX = x;
        lastY = y;
    }

    int centerShift = imageHeight / 2;
    lastX = 0;
    lastY = imageHeight / 4 * 3;

    for (int i = 0; i < imageWidth; i++) {
        float val = 0;
        for (int j = 0; j < pointsPerPixel; j++) {
            val += (rightData[i * pointsPerPixel + j] * 0.5f + 0.5f) * graphHeight;
        }
        val /= pointsPerPixel;

        int x = i;
        int y = (int) val + centerShift;

        painter.drawLine(lastX, lastY, x, y);
        lastX = x;
        lastY = y;
    }
}

void SignalAmplitudeGraph::process(float* leftData,
        float* rightData, int dataSize) {

    this->leftData = leftData;
    this->rightData = rightData;
    this->dataSize = dataSize;

    drawBackImage();

    std::swap(imageFront, imageBack);
    emit queuedRepaint();
}
