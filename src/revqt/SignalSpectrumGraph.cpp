/*
 * SignalSpectrumGraph.cpp
 *
 *  Created on: 17-10-2012
 *      Author: Revers
 */
#include <cmath>
#include <cstring>
#include <iostream>

#include <QLinearGradient>
#include <QApplication>
#include <QPainter>
#include <QImage>
#include <QRect>

#include "SignalSpectrumGraph.h"

#include <rev/common/RevAssert.h>

#include "SignalSpectrumGraph.h"
#include "../RevFFT.h"
#include "../RevComplex.h"

//#define MARGIN 5
#define IMAGE_HEIGHT 128

using namespace rev;
using namespace std;

SignalSpectrumGraph::SignalSpectrumGraph(QWidget* parent) :
        QWidget(parent), bgColor(203, 214, 230) {

    connect(this, SIGNAL(queuedRepaint()), this, SLOT(repaint()),
            Qt::QueuedConnection);

    bars = 128;
    scalingType = ScalingType::DECIBEL;
}

SignalSpectrumGraph::~SignalSpectrumGraph() {
    if (imageBack) {
        delete imageBack;
    }

    if (imageFront) {
        delete imageFront;
    }

    if (fftData) {
        delete[] fftData;
    }

    if (windowData) {
        delete[] windowData;
    }

    if (localFftData) {
        delete[] localFftData;
    }

    if (barsBuffer) {
        delete[] barsBuffer;
    }
}

void SignalSpectrumGraph::paintEvent(QPaintEvent* event) {
    if (!imageFront) {

        QPainter painter(this);
        painter.setPen(bgColor);
        painter.setBrush(QBrush(bgColor));
        painter.drawRect(0, 0, width() - 1, height() - 1);

        return;
    }

    QPainter painter(this);
    painter.drawImage(QRect(0, 0, width() - 1, height() - 1), *imageFront);
}

void SignalSpectrumGraph::fillWindowData() {
    // Hamming Window:
    for (int i = 0; i < fftDataSize; i++) {
        windowData[i] = 0.54f - 0.46f * cos(2.0 * M_PI * i / (fftDataSize - 1));
    }
}

void SignalSpectrumGraph::initBuffers() {
    if (fftData) {
        delete[] fftData;
    }

    if (localFftData) {
        delete[] localFftData;
    }

    if (barsBuffer) {
        delete[] barsBuffer;
    }

    if (windowData) {
        delete[] windowData;
    }
    windowData = new float[fftDataSize];
    fillWindowData();

    fftData = new float[fftDataSize * 2];
    localFftData = new float[fftDataSize * 2];
    barsBuffer = new float[fftDataSize / 2];

    if (imageBack) {
        delete imageBack;
    }
    if (imageFront) {
        delete imageFront;
    }

    imageWidth = 1024;
    barWidth = imageWidth / bars;
    imageHeight = IMAGE_HEIGHT;

    imageBack = new QImage(imageWidth, imageHeight, QImage::Format_RGB32);
    imageFront = new QImage(imageWidth, imageHeight, QImage::Format_RGB32);
    cout << "creating image " << imageWidth << "x" << imageHeight << endl;

    buffersInited = true;
}

void SignalSpectrumGraph::margeAndWindowData() {

    rev::complex* arr = (rev::complex*) fftData;
    float avg = 0;
    for (int i = 0; i < fftDataSize; i++) {
        arr[i].re = (leftData[i] + rightData[i]) * 0.5f;
        arr[i].im = 0.0f;

        avg += arr[i].re;
    }
    avg /= fftDataSize;

    // Remove DC bias and multiply signal by window:
    for (int i = 0; i < fftDataSize; i++) {
        arr[i].re = (arr[i].re - avg); // * windowData[i];
    }

    rev::fft(fftData, fftDataSize);
}

void SignalSpectrumGraph::processSignal() {
    if (!buffersInited) {
        return;
    }

    memcpy(localFftData, fftData, fftDataSize * sizeof(complex));

    complex* arr = (complex*) localFftData;
    int freqStep = sampleRate / fftDataSize;
    //----------------------------------------------------------------------
    // bandfiltering part 1.
    for (int i = 0; i < fftDataSize / 2; i++) {
        int currFreq = i * freqStep;
        if (currFreq < minFrequency || currFreq > maxFrequency) {
            arr[i] = {0.0f, 0.0f};
            arr[fftDataSize - i - 1] = {0.0f, 0.0f};
        } else {
            arr[i] *= gain;
            arr[fftDataSize - i - 1] *= gain;

        }
    }
    //----------------------------------------------------------------------

    // For real valued signals, magnitude M of every freqency component
    // is defined as M = A * (N/2) where A means aplitude of sine component,
    // and N is samples amount. Hence A is given by:
    // A = (2/N) * M
    float amplitudeScaleFactor = 2.0f / fftDataSize;

    int barIndex = 0;
    for (int i = 0; i < fftDataSize / 2; i++) {
        complex& z = arr[i];
        barsBuffer[i] = sqrt(z.re * z.re + z.im * z.im) * amplitudeScaleFactor;
    }

    for (int i = 0, barIndex = 0; i < fftDataSize / 2; i += barWidth, barIndex++) {
        float temp = 0.0f;
        for (int j = 0; j < barWidth; j++) {
            temp += barsBuffer[i + j];
        }
        barsBuffer[barIndex] = temp / barWidth;

        switch (scalingType) {
        case ScalingType::DECIBEL: {
            if (barsBuffer[barIndex] == 0.0f) {
                barsBuffer[barIndex] = 0.0f;
            } else {
                float dbValue = 20.0f * log10((double) barsBuffer[barIndex]);
                barsBuffer[barIndex] = (dbValue - minDBValue)
                        / (maxDBValue - minDBValue);
            }
            break;
        }
        case ScalingType::LINEAR: {
            barsBuffer[barIndex] = (barsBuffer[barIndex] * scaleFactorLinear);
            break;
        }
        case ScalingType::SQRT: {
            barsBuffer[barIndex] = (((sqrt((double) barsBuffer[barIndex]))
                    * scaleFactorSqr));
            break;
        }
        }
    }

    //----------------------------------------------------------------------
    // bandfiltering part 2.
    rev::ifft(localFftData, fftDataSize);
    for (int i = 0; i < fftDataSize; i++) {
        leftData[i] = arr[i].re;
        rightData[i] = arr[i].re;
    }
    //----------------------------------------------------------------------
}

void SignalSpectrumGraph::drawBackImage() {
    if (!buffersInited) {
        return;
    }

    QPainter painter(imageBack);

    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    painter.drawRect(0, 0, imageWidth - 1, imageHeight - 1);

    QPen myPen(Qt::white, 1, Qt::SolidLine);
    painter.setPen(myPen);

    QLinearGradient grad(0, imageHeight / 4, 0, imageHeight);

    grad.setColorAt(0.0, Qt::white);
    grad.setColorAt(1.0, QColor(180, 190, 200));

    painter.setBrush(QBrush(grad));

    for (int i = 0; i < bars; i++) {
        int barHeight = (int) (barsBuffer[i] * (float) imageHeight);
        if (barHeight == 0) {
            continue;
        }

        int x = i * barWidth;
        int y = imageHeight - barHeight;

        painter.drawRect(x, y, barWidth, barHeight);
    }
}

void SignalSpectrumGraph::process(float* leftData, float* rightData, int dataSize) {
    if (dataSize != fftDataSize) {
        fftDataSize = dataSize;

        initBuffers();
    }

    this->leftData = leftData;
    this->rightData = rightData;

    margeAndWindowData();

    processSignal();
    drawBackImage();

    std::swap(imageFront, imageBack);
    emit queuedRepaint();
}

void SignalSpectrumGraph::requestRepaint() {
    processSignal();
    drawBackImage();

    std::swap(imageFront, imageBack);
    emit queuedRepaint();

}
