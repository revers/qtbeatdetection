/* 
 * File:   TestQtAppForm.h
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#ifndef _QTMP3PLAYERWINDOW_H_
#define	_QTMP3PLAYERWINDOW_H_

#include "ui_QtBeatDetectionWindow.h"
#include <rev/audio/RevIAudioOutputStream.h>
#include "../RevIPlayerListener.h"
#include "../RevMP3Player.h"

class QEvent;
class QMouseEvent;
class QImage;
class QStackedWidget;
class QCloseEvent;

namespace rev {
    class SignalAmplitudeGraph;
    class SignalSpectrumGraph;
    class SimpleEnergyBeatDetector;
    class FrequencySelectedBeatDetector;
}

class QtBeatDetectionWindow: public QMainWindow, public rev::IPlayerListener {
Q_OBJECT

private:

    rev::SimpleEnergyBeatDetector* simpleEnergyBeatDetector = nullptr;
    rev::FrequencySelectedBeatDetector* frequencySelectedBeatDetector = nullptr;
    rev::SignalAmplitudeGraph* signalAmplitudeGraph;
    //rev::SignalSpectrumGraph* signalSpectrumGraph;
    Ui::MainWindow widget;
    rev::MP3Player player;
    bool gotHours = false;
    bool gotMinutes = false;
    float msPerSample = 0.0f;
    bool sliderPressed = false;
    int sliderValue = 0;
    bool afterSeek = false;

public:

    QtBeatDetectionWindow(rev::IAudioOutputStreamPtr audioOutputPtr);
    virtual ~QtBeatDetectionWindow();

    /**
     * @Override
     */
    void playEvent(rev::MP3Player* plr) override;

    /**
     * @Override
     */
    void pauseEvent(rev::MP3Player* plr) override;

    /**
     * @Override
     */
    void stopEvent(rev::MP3Player* plr) override;

    void closeEvent(QCloseEvent* event) override;

    /**
     * @Override
     *
     * While playing, notifies every 100ms about current position in samples.
     * If stream has no beginning and no end "currentSamplePos" equals always 0.
     */
    void timeEvent(rev::MP3Player* plr, int currentSamplePos) override;

private:
    void setupActions();
    QString msToString(int ms);

    bool eventFilter(QObject* watched, QEvent* e) override;

    void progressSliderMousePressedEvent(QMouseEvent* event);
    void progressSliderMouseReleasedEvent(QMouseEvent* event);

private slots:
    void openAction();
    void aboutAction();
    void playAction();
    void pauseAction();
    void stopAction();

    void progressSliderValueChangedEvent(int value);
    void stopSlot();
    void pauseSlot();
    void playSlot();

    void timeSlot(unsigned int samplePos);

    void beatDetectionRadioChecked(bool checked);

    void simpleCoeffAChanged(double val);
    void simpleCoeffBChanged(double val);

    void freqCoeffAChanged(double val);
    void freqCoeffBChanged(double val);
    void freqBandsThresholdChanged(int val);

signals:

    void repaintGraphsSignal();
    void stopSignal();
    void pauseSignal();
    void playSignal();
    void timeSignal(unsigned int samplePos);
};

#endif	/* _QTMP3PLAYERWINDOW_H_ */
