/*
 * SignalAmplitudeGraph.h
 *
 *  Created on: 16-10-2012
 *      Author: Revers
 */

#ifndef SIGNALAMPLITUDEGRAPH_H_
#define SIGNALAMPLITUDEGRAPH_H_

#include <ctime>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "../RevIDSP.h"
#include "../RevIBeatListener.h"

class QImage;
class QTimerEvent;

namespace rev {

    class SignalAmplitudeGraph: public QWidget, public IDSP, public IBeatListener {
    Q_OBJECT

        QColor bgColor;
        QColor circleColor;
        QImage* imageBack = nullptr;
        QImage* imageFront = nullptr;
        float* leftData = nullptr;
        float* rightData = nullptr;
        int imageWidth = 0;
        int imageHeight = 0;
        int dataSize = 0;

        bool canEmitRepaint = true;
        clock_t animStartTime = 0;
        clock_t animTotalTime = 250;
    public:

        SignalAmplitudeGraph(QWidget* parent = 0);

        virtual ~SignalAmplitudeGraph();

        void process(float* leftData, float* rightData,
                int dataSize) override;

        void beat(const float* leftData, const float* rightData,
                        int dataSize) override {
            startCircleAnimation();
        }

        void startCircleAnimation() {
            animStartTime = clock() + 20;
        }

    protected:

        void timerEvent(QTimerEvent* event) override;

        void paintEvent(QPaintEvent* event) override;

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }

    public slots:
        void requestRepaint();

    signals:
        void queuedRepaint();

    private:

        void drawBackImage();
        void drawCircle(clock_t diffTime, QPainter& painter);
    };

} /* namespace rev */
#endif /* SIGNALAMPLITUDEGRAPH_H_ */
