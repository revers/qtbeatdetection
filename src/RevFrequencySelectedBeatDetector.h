/*
 * RevFrequencySelectedBeatDetector.h
 *
 *  Created on: 07-11-2012
 *      Author: Revers
 */

#ifndef REVFREQUENCYSELECTEDBEATDETECTOR_H_
#define REVFREQUENCYSELECTEDBEATDETECTOR_H_

#include <cstring>
#include <rev/common/RevAssert.h>
#include "RevAbstractBeatDetector.h"

namespace rev {

    class FrequencySelectedBeatDetector: public AbstractBeatDetector {
        static const int MAX_BAND_WIDTHS = 1024;

        int packetsRead = 0;
        AudioFormat format;
        int bands = 0;

        int bandWidths[MAX_BAND_WIDTHS];
        float* historyBuffers[MAX_BAND_WIDTHS];
        int historyBuffersSize = 0;
        //bool inited = false;

        int fftDataSize = 0;
        /**
         * Array of size 2 * fftDataSize.
         */
        float* fftData = nullptr;
        float* fftMagnitudeData = nullptr;
        float* windowData = nullptr;

        float coefficientA = 0.0025711f;//10.0025714f;
        float coefficientB = 1.5152857f;//1.5142857f;
        int bandThreshold = 2;

    public:
        FrequencySelectedBeatDetector(IBeatListener* beatListener);

        virtual ~FrequencySelectedBeatDetector();

        void process(float* leftData, float* rightData,
                int dataSize) override;

        bool init(AudioFormat format, int framesAtOnce) override;

        void reset() override {
            packetsRead = 0;
        }

        float getCoefficientA() const {
            return coefficientA;
        }

        void setCoefficientA(float coefficientA) {
            this->coefficientA = coefficientA;
        }

        float getCoefficientB() const {
            return coefficientB;
        }

        void setCoefficientB(float coefficientB) {
            this->coefficientB = coefficientB;
        }

        int getBands() const {
            return bands;
        }

        int getBandThreshold() const {
            return bandThreshold;
        }

        void setBandThreshold(int bandThreshold) {
            this->bandThreshold = bandThreshold;
        }

    private:
        void fillWindowData();
        bool processOneBand(float* historyBuffer, int historyBufferSize,
                int packetsRead, float* data, int dataSize);

    };

} /* namespace rev */
#endif /* REVFREQUENCYSELECTEDBEATDETECTOR_H_ */
