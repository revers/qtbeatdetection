/*
 * RevFrequencySelectedBeatDetector.cpp
 *
 *  Created on: 07-11-2012
 *      Author: Revers
 */

#include <iostream>
#include <rev/common/RevErrorStream.h>

#include "RevFFT.h"
#include "RevComplex.h"
#include "RevFrequencySelectedBeatDetector.h"

using namespace rev;
using namespace std;

#define BANDS_TRIGGERED_FOR_BEAT 3
#define HISTORY_BUFFER_MULTIPLIER 4

FrequencySelectedBeatDetector::FrequencySelectedBeatDetector(
        IBeatListener* beatListener) :
        AbstractBeatDetector(beatListener) {
    bandThreshold = BANDS_TRIGGERED_FOR_BEAT;
}

FrequencySelectedBeatDetector::~FrequencySelectedBeatDetector() {
//    if (historyBuffer) {
//        delete[] historyBuffer;
//    }
}

#define PRC(x, y) ((float)x/(float)y * 100.0f)

bool FrequencySelectedBeatDetector::init(AudioFormat format, int framesAtOnce) {
    this->format = format;
    this->fftDataSize = framesAtOnce;

    if (fftData) {
        delete[] fftData;
    }
    if (windowData) {
        delete[] windowData;
    }
    if (fftMagnitudeData) {
        delete[] fftMagnitudeData;
    }

    fftData = new float[fftDataSize * 2];
    fftMagnitudeData = new float[fftDataSize];
    windowData = new float[fftDataSize];
    fillWindowData();
    //---------------------------------------------
    int hzBands[] { 200, 400, 800, 1600, 3200 };
    bands = sizeof(hzBands) / sizeof(int) + 1;
    
    historyBuffersSize = (int) ((float) format.sampleRate * 1.0f / fftDataSize
            + 0.5f);

    REV_TRACE_MSG("History buffer size: " << historyBuffersSize);
    for (int i = 0; i < bands; i++) {
        historyBuffers[i] =
                new float[HISTORY_BUFFER_MULTIPLIER * historyBuffersSize];
    }

    float oneElementHz = (float) format.sampleRate / (float) framesAtOnce;

    int elementSum = 0;
    for (int i = 0; i < bands - 1; i++) {
        bandWidths[i] = (int) (float(hzBands[i]) / oneElementHz + 0.5f);
        if (i > 0) {
            bandWidths[i] -= bandWidths[i - 1];
        }
        elementSum += bandWidths[i];
        printf("bands[%d] = %d (%.2f%%)\n", i, bandWidths[i], PRC(bandWidths[i], (framesAtOnce/2)));
    }
    bandWidths[bands - 1] = framesAtOnce / 2 - elementSum;
    printf("bands[%d] = %d (%.2f%%)\n", bands - 1, bandWidths[bands - 1], PRC(bandWidths[bands - 1], (framesAtOnce/2)));
    elementSum += bandWidths[bands - 1];
    cout << a2s(R(bands), R(framesAtOnce), R(elementSum)) << endl;

    fflush(stdout);

    assert(bands <= MAX_BAND_WIDTHS);

    return true;
}

bool FrequencySelectedBeatDetector::processOneBand(float* historyBuffer,
        int historyBufferSize, int packetsRead, float* data, int dataSize) {

    float instantEnergy = 0.0f;
    for (int i = 0; i < dataSize; i++) {
        instantEnergy += data[i];
    }
    instantEnergy /= dataSize;

    if (packetsRead < historyBufferSize) {
        historyBuffer[packetsRead] = instantEnergy;
        return false;
    }

    float* historyBegin = historyBuffer + packetsRead
            - historyBufferSize;

    float averageLocalEnergy = 0.0f;
    for (int i = 0; i < historyBufferSize; i++) {
        averageLocalEnergy += historyBegin[i];
    }

    averageLocalEnergy /= historyBufferSize;

    float variance = 0.0f;
    for (int i = 0; i < historyBufferSize; i++) {
        float diff = historyBegin[i] - averageLocalEnergy;
        variance += diff * diff;
    }
    variance /= historyBufferSize;
    //float c = 250.0f;
    float c = (coefficientA * variance) + coefficientB;

    if (packetsRead < HISTORY_BUFFER_MULTIPLIER * historyBufferSize) {
        historyBuffer[packetsRead] = instantEnergy;
    } else {
        memcpy(historyBuffer, historyBuffer + historyBufferSize + 1,
                (historyBufferSize - 1) * sizeof(float));
        historyBuffer[historyBufferSize - 1] = instantEnergy;
        // packetsRead = historyBufferSize;
    }

    if (instantEnergy > c * averageLocalEnergy) {
        return true;
    }
    return false;
}

void FrequencySelectedBeatDetector::process(float* leftData, float* rightData,
        int dataSize) {

    rev::complex* arr = (rev::complex*) fftData;
    float avg = 0;
    for (int i = 0; i < fftDataSize; i++) {
        arr[i].re = (leftData[i] + rightData[i]) * 0.5f;
        arr[i].im = 0.0f;

        avg += arr[i].re;
    }
    avg /= fftDataSize;

    // Remove DC bias and multiply signal by window:
    for (int i = 0; i < fftDataSize; i++) {
        arr[i].re = (arr[i].re - avg) * windowData[i];
    }

    rev::fft(fftData, fftDataSize);

    // For real valued signals, magnitude M of every freqency component
    // is defined as M = A * (N/2) where A means aplitude of sine component,
    // and N is samples amount. Hence A is given by:
    // A = (2/N) * M
    float amplitudeScaleFactor = 1.0f / fftDataSize;

    for (int i = 0; i < fftDataSize / 2; i++) {
        complex& z = arr[i];
        fftMagnitudeData[i] = sqrt(z.re * z.re + z.im * z.im)
                * amplitudeScaleFactor;
    }
    //----------------------------------------------------------------------------

    if (packetsRead >= HISTORY_BUFFER_MULTIPLIER * historyBuffersSize) {
        packetsRead = historyBuffersSize;
    }

    if (packetsRead < historyBuffersSize) {
        int dataShift = 0;
        for (int i = 0; i < bands; i++) {
            int size = bandWidths[i];
            processOneBand(historyBuffers[i], historyBuffersSize, packetsRead,
                    fftMagnitudeData + dataShift, size);
            dataShift += size;
        }
    } else {
        int dataShift = 0;
        int bandsTriggered = 0;
        for (int i = 0; i < bands; i++) {
            int size = bandWidths[i];
            if (processOneBand(historyBuffers[i], historyBuffersSize,
                    packetsRead, fftMagnitudeData + dataShift, size)) {
                bandsTriggered++;
            }

            if (bandsTriggered >= bandThreshold) {
                beatListener->beat(leftData, rightData, dataSize);
                break;
            }
            dataShift += size;
        }
    }

    packetsRead++;
}

void FrequencySelectedBeatDetector::fillWindowData() {
    // Hamming Window:
    for (int i = 0; i < fftDataSize; i++) {
        windowData[i] = 0.54f - 0.46f * cos(2.0 * M_PI * i / (fftDataSize - 1));
    }
}
