/*
 * RevIBeatListener.h
 *
 *  Created on: 05-11-2012
 *      Author: Revers
 */

#ifndef REVIBEATLISTENER_H_
#define REVIBEATLISTENER_H_

namespace rev {

    class IBeatListener {
    public:
        IBeatListener() {
        }

        virtual ~IBeatListener() {
        }

        virtual void beat(const float* leftData, const float* rightData,
                int dataSize) = 0;
    };
}

#endif /* REVIBEATLISTENER_H_ */
