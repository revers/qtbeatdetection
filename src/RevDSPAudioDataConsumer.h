/*
 * DSPAudioDataConsumer.h
 *
 *  Created on: 13-10-2012
 *      Author: Revers
 */

#ifndef REVDSPAUDIODATACONSUMER_H_
#define REVDSPAUDIODATACONSUMER_H_

#include <memory>
#include <rev/audio/RevAudioFormat.h>
#include <rev/audio/RevIAudioOutputStream.h>

#include "RevIAudioDataConsumer.h"
#include "RevIDSP.h"

namespace rev {

typedef std::shared_ptr<IAudioOutputStream> IAudioOutputStreamPtr;
typedef std::vector<rev::IDSP*> DSPVector;
typedef DSPVector::iterator DSPVectorIter;

class DSPAudioDataConsumer: public IAudioDataConsumer {
	rev::AudioFormat inputFormat;
	rev::AudioFormat outputFormat;

	bool inited = false;
	unsigned char* dataBuffer = nullptr;
	int dataBufferLength = 0;
	int dataBufferCurrentOffset = 0;
	int dataBufferFirstFrameOffset = 0;
	int consumedDataLength = 0;

	int framesAtOnce = 0;
	int framesAtOnceSize = 0;
	float* leftData = nullptr;
	float* rightData = nullptr;

	float* outputBuffer = nullptr;
	int outputBufferSize = 0;

	int frameSize = 0;
	bool active = false;

	IDSP* lastDSP = nullptr;
	DSPVector dspVector;
	IAudioOutputStreamPtr outputStreamPtr;

public:

	DSPAudioDataConsumer(IAudioOutputStreamPtr outputStreamPtr);

	virtual ~DSPAudioDataConsumer();

	bool init(rev::AudioFormat format, int framesAtOnce) override;

	void consume(unsigned char* dataArray, int dataArrayLength, bool playing)
			override;

	void flush() override;
	void start() override;
	void stop() override;

	bool isActive() override {
		return active;
	}

	DSPVector& getDSPVector() {
		return dspVector;
	}

	void addDSP(rev::IDSP* dsp) {
		dspVector.push_back(dsp);
	}

	IDSP* getLastDSP() {
		return lastDSP;
	}

	void setLastDSP(IDSP* lastDSP) {
		this->lastDSP = lastDSP;
	}

private:
	void clean();

	int calculateSamplePosition();

	void extractSamples();

	void extractSamplesFloat32Mono();
	void extractSamplesFloat32Stereo();

	void padWithZeros();

	/******************************************************
	 * extractSamplesSignedMono
	 ******************************************************/
	template<typename T>
	void extractSamplesSignedMono(float maxAbsoluteValue) {
		for (int i = 0; i < framesAtOnce; i++) {
			int offset = (dataBufferFirstFrameOffset + i * frameSize)
					% dataBufferLength;
			T* buffer = reinterpret_cast<T*>(dataBuffer + offset);

			leftData[i] = (float) (*buffer) / maxAbsoluteValue;
			rightData[i] = leftData[i];
		}
	}

	/******************************************************
	 * extractSamplesSignedStereo
	 ******************************************************/
	template<typename T>
	void extractSamplesSignedStereo(float maxAbsoluteValue) {
		typedef struct {
			T left;
			T right;
		} StereoStruct;

		for (int i = 0; i < framesAtOnce; i++) {
			int offset = (dataBufferFirstFrameOffset + i * frameSize)
					% dataBufferLength;
			StereoStruct* buffer = reinterpret_cast<StereoStruct*>(dataBuffer
					+ offset);

			leftData[i] = (float) buffer->left / maxAbsoluteValue;
			rightData[i] = (float) buffer->right / maxAbsoluteValue;
		}
	}

	/******************************************************
	 * extractSamplesUnsignedMono
	 ******************************************************/
	template<typename T>
	void extractSamplesUnsignedMono(float halfMax) {
		for (int i = 0; i < framesAtOnce; i++) {
			int offset = (dataBufferFirstFrameOffset + i * frameSize)
					% dataBufferLength;
			T* buffer = reinterpret_cast<T*>(dataBuffer + offset);

			leftData[i] = ((float) (*buffer) - halfMax) / halfMax;
			rightData[i] = leftData[i];
		}
	}

	/******************************************************
	 * extractSamplesUnsignedStereo
	 ******************************************************/
	template<typename T>
	void extractSamplesUnsignedStereo(float halfMax) {
		typedef struct {
			T left;
			T right;
		} StereoStruct;

		for (int i = 0; i < framesAtOnce; i++) {
			int offset = (dataBufferFirstFrameOffset + i * frameSize)
					% dataBufferLength;
			StereoStruct* buffer = reinterpret_cast<StereoStruct*>(dataBuffer
					+ offset);

			leftData[i] = ((float) buffer->left - halfMax) / halfMax;
			rightData[i] = ((float) buffer->right - halfMax) / halfMax;
		}
	}

	/******************************************************
	 * padWithZerosMono
	 ******************************************************/
	template<typename T>
	void padWithZerosMono() {
		for (int i = consumedDataLength / frameSize; i < framesAtOnce; i++) {
			int offset = (dataBufferFirstFrameOffset + i * frameSize)
					% dataBufferLength;
			T* buffer = reinterpret_cast<T*>(dataBuffer + offset);
			*buffer = T(0);
		}
	}

	/******************************************************
	 * padWithZerosStereo
	 ******************************************************/
	template<typename T>
	void padWithZerosStereo() {
		typedef struct {
			T left;
			T right;
		} StereoStruct;

		for (int i = consumedDataLength / frameSize; i < framesAtOnce; i++) {
			int offset = (dataBufferFirstFrameOffset + i * frameSize)
					% dataBufferLength;
			StereoStruct* buffer = reinterpret_cast<StereoStruct*>(dataBuffer
					+ offset);
			*buffer = {T(0), T(0)};
		}
	}
};

}
/* namespace rev */
#endif /* REVDSPAUDIODATACONSUMER_H_ */
