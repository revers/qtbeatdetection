/*
 * RevDSPAudioDataConsumer.cpp
 *
 *  Created on: 13-10-2012
 *      Author: Revers
 */

#include <iostream>
#include <cstring>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevArrayUtil.h>

#include "RevDSPAudioDataConsumer.h"

using namespace std;
using namespace rev;

DSPAudioDataConsumer::DSPAudioDataConsumer(IAudioOutputStreamPtr outputStreamPtr) :
		outputStreamPtr(outputStreamPtr) {
}

DSPAudioDataConsumer::~DSPAudioDataConsumer() {
	clean();
}

bool DSPAudioDataConsumer::init(rev::AudioFormat format, int framesAtOnce) {
	REV_ERROR_MSG(
			rev::a2s(R(format.encoding), R(format.channels), R(format.sampleRate), R(format.getBytesPerSample()), R(format.getFrameSize())));
	clean();

	if (format.channels != 1 && format.channels != 2) {
		inited = false;
		REV_ERROR_MSG(
				"ERROR: Unsupported channel number: " << format.channels << ". Only MONO and STEREO are supported.");
		return false;
	}

	this->outputFormat.set(format.sampleRate, 2, AudioEncoding::FLOAT_32);
	outputBufferSize = outputFormat.channels * sizeof(float) * framesAtOnce;
	outputBuffer = new float[outputBufferSize];

	if (!outputStreamPtr->open(outputFormat)) {
		REV_ERROR_MSG("ERROR: cannot create audio output stream!!");
		return false;
	}

	this->inputFormat = format;
	this->frameSize = format.getFrameSize();
	this->framesAtOnce = framesAtOnce;
	this->framesAtOnceSize = format.getFrameSize() * framesAtOnce;
	// reserve twice the needed space.
	dataBufferLength = framesAtOnceSize * 2;

	dataBuffer = new unsigned char[dataBufferLength];
	leftData = new float[framesAtOnce];
	rightData = new float[framesAtOnce];

	inited = true;

	return true;
}

void DSPAudioDataConsumer::flush() {
}

void DSPAudioDataConsumer::start() {
	assert(inited);
	active = true;
}

void DSPAudioDataConsumer::stop() {
	if (!inited) {
		return;
	}

	flush();
}

void DSPAudioDataConsumer::consume(unsigned char* dataArray,
		int dataArrayLength, bool playing) {
	assert(inited);

	int nextOffset = dataBufferCurrentOffset + dataArrayLength;

	if (nextOffset >= dataBufferLength) {
		int overrunLength = nextOffset - dataBufferLength;
		int remainderLength = dataArrayLength - overrunLength;

		memcpy(dataBuffer + dataBufferCurrentOffset, dataArray, remainderLength);
		memcpy(dataBuffer, dataArray + remainderLength, overrunLength);

		dataBufferCurrentOffset = overrunLength;

	} else {
		memcpy(dataBuffer + dataBufferCurrentOffset, dataArray, dataArrayLength);
		dataBufferCurrentOffset = nextOffset % dataBufferLength;
	}

	consumedDataLength += dataArrayLength;
	if (consumedDataLength >= framesAtOnceSize) {

		int diff = dataBufferCurrentOffset - consumedDataLength;
		dataBufferFirstFrameOffset =
				(diff < 0) ? dataBufferLength + diff : diff;

		consumedDataLength -= framesAtOnceSize;

		extractSamples();

		for (auto& dsp : dspVector) {
			dsp->process(leftData, rightData, framesAtOnce);
		}

		if (lastDSP) {
			lastDSP->process(leftData, rightData, framesAtOnce);
		}

		if (playing) {
			for (int i = 0; i < framesAtOnce; i++) {
				outputBuffer[i * 2] = leftData[i];
				outputBuffer[i * 2 + 1] = rightData[i];
			}

			outputStreamPtr->write((unsigned char*) outputBuffer, outputBufferSize);
		}
	}
}

void DSPAudioDataConsumer::extractSamples() {

	switch (inputFormat.encoding) {
	case rev::AudioEncoding::FLOAT_32: {
		if (inputFormat.channels == 2) {
			extractSamplesFloat32Stereo();
		} else { // inputFormat.channels == 1
			extractSamplesFloat32Mono();
		}
		break;
	}
	case rev::AudioEncoding::SIGNED_16: {
		if (inputFormat.channels == 2) {
			extractSamplesSignedStereo<signed short>(32767.0f);
		} else { // inputFormat.channels == 1
			extractSamplesSignedMono<signed short>(32767.0f);
		}
		break;
	}
	case rev::AudioEncoding::UNSIGNED_16: {
		if (inputFormat.channels == 2) {
			extractSamplesUnsignedStereo<unsigned short>(32767.0f);
		} else { // inputFormat.channels == 1
			extractSamplesUnsignedMono<unsigned short>(32767.0f);
		}
		break;
	}
	case rev::AudioEncoding::SIGNED_8: {
		if (inputFormat.channels == 2) {
			extractSamplesSignedStereo<signed char>(128.0f);
		} else { // inputFormat.channels == 1
			extractSamplesSignedMono<signed char>(128.0f);
		}
		break;
	}
	case rev::AudioEncoding::UNSIGNED_8: {
		if (inputFormat.channels == 2) {
			extractSamplesUnsignedStereo<unsigned char>(128.0f);
		} else { // inputFormat.channels == 1
			extractSamplesUnsignedMono<unsigned char>(128.0f);
		}
		break;
	}
	default: {
		alwaysAssertMsg(false, "Unknown AudioEncoding inputFormat!!");
		break;
	}
	}
}

void DSPAudioDataConsumer::clean() {

	if (dataBuffer) {
		delete[] dataBuffer;
		dataBuffer = nullptr;
	}
	if (leftData) {
		delete[] leftData;
		leftData = nullptr;
	}
	if (rightData) {
		delete[] rightData;
		rightData = nullptr;
	}

	if (outputBuffer) {
		delete[] outputBuffer;
	}

	dataBufferLength = 0;
	dataBufferCurrentOffset = 0;
	dataBufferFirstFrameOffset = 0;
	consumedDataLength = 0;

	inited = false;
	framesAtOnce = 0;
	framesAtOnceSize = 0;
	frameSize = 0;
}

void DSPAudioDataConsumer::padWithZeros() {
	switch (inputFormat.encoding) {
	case rev::AudioEncoding::FLOAT_32: {
		if (inputFormat.channels == 2) {
			padWithZerosStereo<float>();
		} else { // inputFormat.channels == 1
			padWithZerosMono<float>();
		}
		break;
	}
	case rev::AudioEncoding::SIGNED_16: {
		if (inputFormat.channels == 2) {
			padWithZerosStereo<signed short>();
		} else { // inputFormat.channels == 1
			padWithZerosMono<signed short>();
		}
		break;
	}
	case rev::AudioEncoding::UNSIGNED_16: {
		if (inputFormat.channels == 2) {
			padWithZerosStereo<unsigned short>();
		} else { // inputFormat.channels == 1
			padWithZerosMono<unsigned short>();
		}
		break;
	}
	case rev::AudioEncoding::SIGNED_8: {
		if (inputFormat.channels == 2) {
			padWithZerosStereo<signed char>();
		} else { // inputFormat.channels == 1
			padWithZerosMono<signed char>();
		}
		break;
	}
	case rev::AudioEncoding::UNSIGNED_8: {
		if (inputFormat.channels == 2) {
			padWithZerosStereo<unsigned char>();
		} else { // inputFormat.channels == 1
			padWithZerosMono<unsigned char>();
		}
		break;
	}

	default: {
		alwaysAssertMsg(false, "Unknown AudioEncoding inputFormat!!");
		break;
	}
	}
}

void DSPAudioDataConsumer::extractSamplesFloat32Mono() {
	for (int i = 0; i < framesAtOnce; i++) {
		int offset = (dataBufferFirstFrameOffset + i * frameSize)
				% dataBufferLength;
		float* buffer = reinterpret_cast<float*>(dataBuffer + offset);

		leftData[i] = *buffer;
		rightData[i] = *buffer;
	}
}
void DSPAudioDataConsumer::extractSamplesFloat32Stereo() {
	typedef struct {
		float left;
		float right;
	} StereoStruct;

	for (int i = 0; i < framesAtOnce; i++) {
		int offset = (dataBufferFirstFrameOffset + i * frameSize)
				% dataBufferLength;
		StereoStruct* buffer = reinterpret_cast<StereoStruct*>(dataBuffer + offset);

		leftData[i] = buffer->left;
		rightData[i] = buffer->right;
	}
}
