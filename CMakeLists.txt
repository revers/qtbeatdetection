cmake_minimum_required (VERSION 2.6)
 
project (QtBeatDetection)

IF(CMAKE_BUILD_TYPE STREQUAL Release) 
    SET(REV_RELEASE_MODE ON)
ELSEIF(CMAKE_BUILD_TYPE STREQUAL Debug) 
	SET(REV_DEBUG_MODE ON)
ENDIF()

IF(REV_RELEASE_MODE) 
	ADD_DEFINITIONS(-DNDEBUG)
ENDIF()

# QT4 Handling
FIND_PACKAGE(Qt4 REQUIRED)

INCLUDE(${QT_USE_FILE})

#---------------------------------------------------------------------------
macro (VerboseCopyFile src dest) 
	message(STATUS "COPYING (IF NEEDED) FILE ${src} TO ${dest}")
	configure_file("${src}" "${dest}" COPYONLY)
endmacro (VerboseCopyFile)
#---------------------------------------------------------------------------
macro (addAllFiles filePattern destDir)
	FILE( GLOB files_temp  ${filePattern})
	foreach(loop_var ${files_temp})
		
		get_filename_component(my_name ${loop_var} NAME)
		VerboseCopyFile("${loop_var}" "${destDir}/${my_name}")
		
	endforeach(loop_var)
endmacro (addAllFiles)
#---------------------------------------------------------------------------
IF(REV_DEBUG_MODE) 	
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/DEBUG/*.dll"
		"${CMAKE_BINARY_DIR}"
	)
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevAudio/build/DEBUG/*.dll"
		"${CMAKE_BINARY_DIR}"
	)
ELSEIF(REV_RELEASE_MODE)
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/RELEASE/*.dll"
		"${CMAKE_BINARY_DIR}"
	)
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevAudio/build/RELEASE/*.dll"
		"${CMAKE_BINARY_DIR}"
	)
ENDIF()	
#---------------------------------------------------------------------------
VerboseCopyFile("${PROJECT_SOURCE_DIR}/../lib/libmpg123-0.dll" "${CMAKE_BINARY_DIR}")
VerboseCopyFile("${PROJECT_SOURCE_DIR}/../lib/OpenAL32.dll" "${CMAKE_BINARY_DIR}")
VerboseCopyFile("${PROJECT_SOURCE_DIR}/../lib/libresample.dll" "${CMAKE_BINARY_DIR}")
VerboseCopyFile("${PROJECT_SOURCE_DIR}/../lib/portaudio_x86.dll" "${CMAKE_BINARY_DIR}")
#---------------------------------------------------------------------------
#addAllFiles(
#	"${PROJECT_SOURCE_DIR}/src/*.properties"
#	"${CMAKE_BINARY_DIR}")
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/*.dll"
	"${CMAKE_BINARY_DIR}")
#---------------------------------------------------------------------------
include_directories (
	"${PROJECT_SOURCE_DIR}/../RevCommon/include"
    "${PROJECT_SOURCE_DIR}/../RevAudio/include"
	"${PROJECT_SOURCE_DIR}/../include"
	"${CMAKE_BINARY_DIR}"
	#"${PROJECT_SOURCE_DIR}/../qt-4.8.2/include"
)
#---------------------------------------------------------------------------
IF(REV_DEBUG_MODE) 	
	LINK_DIRECTORIES(
		"C:/MinGW/lib"
        "${PROJECT_SOURCE_DIR}/../RevAudio/build/DEBUG"
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/DEBUG"
		"${PROJECT_SOURCE_DIR}/../lib"
	) 
	SET(PLATFORM_LIBS RevAudioD RevCommonD boost_thread boost_chrono boost_system ${QT_LIBRARIES})
		
ELSEIF(REV_RELEASE_MODE)
		LINK_DIRECTORIES(
		"C:/MinGW/lib"
        "${PROJECT_SOURCE_DIR}/../RevAudio/build/RELEASE"
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/RELEASE"
		"${PROJECT_SOURCE_DIR}/../lib"
	) 
	
	SET(PLATFORM_LIBS RevAudio RevCommon boost_thread boost_chrono boost_system ${QT_LIBRARIES})
ENDIF()	
#---------------------------------------------------------------------------

FILE( GLOB REV_QT4_SRC_H "${PROJECT_SOURCE_DIR}/src/revqt/*.h" )
FILE( GLOB REV_QT4_SRC_CPP "${PROJECT_SOURCE_DIR}/src/revqt/*.cpp" )
FILE( GLOB REV_QT4_UI  "${PROJECT_SOURCE_DIR}/src/revqt/*.ui" )
SET( REV_QT4_RES )

QT4_WRAP_CPP(REV_MOC_CPP ${REV_QT4_SRC_H})
QT4_WRAP_UI(REV_UI_CPP ${REV_QT4_UI})
QT4_ADD_RESOURCES(REV_RES_H ${REV_QT4_RES})

FILE( GLOB REV_FILES src/*.h src/*.cpp )

SET( REV_SRC
	${REV_FILES}
	${REV_QT4_SRC_CPP}
	${REV_MOC_CPP}
	${REV_UI_CPP}
	${REV_RES_H}
)

add_executable(QtBeatDetection ${REV_SRC}) 
TARGET_LINK_LIBRARIES(QtBeatDetection ${PLATFORM_LIBS})